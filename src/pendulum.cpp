/**
 * @file pendulum.cpp
 * @brief Pendulum control and estimation
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "pendulum/pendulum.h"
#include "iostream"

namespace acl {
namespace pendulum {

Pendulum::Pendulum()
{
  quat_ = Eigen::Quaterniond::Identity();
}

// ----------------------------------------------------------------------------

void Pendulum::setParameters(const Params& params)
{
  params_ = params;
}

// ----------------------------------------------------------------------------

void Pendulum::estimateAngle(uint64_t time_us, const Eigen::Vector3f& acc,
                            const Eigen::Vector3f& gyr)
{
  static uint64_t last_time_us = time_us;

  // initialize filter with first measurement
  if (!lpf_initialized_) {
    acc_ = acc;
    gyr_ = gyr;
    lpf_initialized_ = true;
  }

  // lowpass filter input measurements
  LPF(params_.lpf_acc_alpha, acc, acc_);
  LPF(params_.lpf_gyr_alpha, gyr, gyr_);

  //
  // Estimate orientation from gyro
  //

  const double dt = (time_us - last_time_us)*1e-6;

  quat_.w() -= 0.5*(quat_.x()*gyr.x() + quat_.y()*gyr.y() + quat_.z()*gyr.z())*dt;
  quat_.x() += 0.5*(quat_.w()*gyr.x() - quat_.z()*gyr.y() + quat_.y()*gyr.z())*dt;
  quat_.y() += 0.5*(quat_.z()*gyr.x() + quat_.w()*gyr.y() - quat_.x()*gyr.z())*dt;
  quat_.z() += 0.5*(quat_.x()*gyr.y() - quat_.y()*gyr.x() + quat_.w()*gyr.z())*dt;
  quat_.normalize();

  // TODO: use accelerometer in complementary fashion
  last_time_us = time_us;
  if (running_ && time_start_ == 0){
    time_start_ = time_us;
  }
  tnow_ = time_us - time_start_;


  //
  // Extract angle
  //

  // for convenience, extract the angle about the axis of rotation (x-axis)
  // Eigen::Vector3d euler = quat_.toRotationMatrix().eulerAngles(2, 1, 0);
  //angle conversion
  float t1, t2;
  t1 = 2.0 * (quat_.w() * quat_.x() + quat_.y() * quat_.z());
  t2 = 1.0 - 2.0 * (quat_.x() * quat_.x() + quat_.y() * quat_.y());
  float ex = atan2(t1, t2);
  t2 = 2.0 * (quat_.w() * quat_.y() - quat_.z() * quat_.x());
  if (t2>1){t2 = 1;}
  if (t2<-1){t2 = -1;}
  float ey = asin(t2);
  t1 = 2.0 * (quat_.w() * quat_.z() + quat_.x() * quat_.y());
  t2 = 1.0 - 2.0 * (quat_.y()*quat_.y() + quat_.z()*quat_.z());
  float ez = atan2(t1, t2);
  if (ez > M_PI){
    ez = ez - 2*M_PI;
  }

  d_theta_ = gyr.x();
  if (ex - theta_ < -M_PI){
    theta_ = ex + 2 * M_PI;
  }
  else if (ex - theta_ > M_PI) {
    theta_ = ex - 2 * M_PI;
  }
  else {
    theta_ = ex;
  }

}

// ----------------------------------------------------------------------------
//
// void Pendulum::quaternion_to_euler(){
//   float t1, t2;
//   t1 = 2.0 * (q.w() * q.x() + q.y() * q.z());
//   t2 = 1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
//   euler.x() = 180/M_PI *atan2(t1, t2);
//   t2 = 2.0 * (q.w() * q.y() - q.z() * q.x());
//   if (t2>1){t2 = 1;}
//   if (t2<-1){t2 = -1;}
//   euler.y() = 180/M_PI *asin(t2);
//   t1 = 2.0 * (q.w() * q.z() + q.x() * q.y());
//   t2 = 1.0 - 2.0 * (q.y()*q.y() + q.z()*q.z());
//   euler.z() = 180/M_PI *atan2(t1, t2);
//   if (euler.z() > 180){
//     euler.z() = euler.z() - 360;
//   }
// }

void Pendulum::run(){
  running_ = true;
}

void Pendulum::stopRunning(){
  running_ = false;
  time_start_ = 0;
}
void Pendulum::setMultip(float a){ params_.multip = a;}
void Pendulum::setLambda(float a){params_.lambd = a;}
void Pendulum::setNu(float a){params_.nu = a;}
void Pendulum::setF(float a){params_.F = a;}
void Pendulum::setD(float a){params_.D = a;}
void Pendulum::setThetaDes(float a){theta_des_ = a;}
void Pendulum::setM6(float a){params_.m6 = a;}
void Pendulum::setM7(float a){params_.m7 = a;}

void Pendulum::computePID()
{
  computeGravity();
  u_ = pid_.Kp * (theta_des()-theta_) - pid_.Kd * d_theta_;
  // std::cout << pid_.Kp << "\t" << pid_.Ki << "\t" << pid_.Kd << "\t" << pid_.ff << std::endl;
  addUp();
}

int Pendulum::lookup(int m_num, float thrust){
  // std::cout << m_num << "\t" << thrust << std::endl;
  int i = 0;
  if (m_num == 6){thrust *= params_.m6;}
  if (m_num == 7){thrust *= params_.m7;}

  if (thrust >= pq_.umax){ thrust = pq_.umax; }

  while (thrust_[i] < thrust){i+=1;}
  if (i == 0){
    if (m_num == 6) {return esc_m6_[0]-3;}
    if (m_num == 7) {return esc_m7_[0]-3;}
  }
  if (m_num == 6) {
    return esc_m6_[i-1] + (esc_m6_[i] - esc_m6_[i-1]) * (thrust-thrust_[i-1]) / (thrust_[i] - thrust_[i-1]);
  }
  if (m_num == 7) {
    return esc_m7_[i-1] + (esc_m7_[i] - esc_m7_[i-1]) * (thrust-thrust_[i-1]) / (thrust_[i] - thrust_[i-1])+ pid_.ff;
  }

}


void Pendulum::addUp() {
  if (u_ < 0) {
    u_neg_ += -u_;
  }
  else if (u_ > 0) {
    u_pos_ += u_;
  }
  u_ = 0.0;
}

Eigen::Vector3d Pendulum::computeControl(uint16_t *pwm) {
  u_ = 0.0; u_neg_ = 0.0; u_pos_ = 0.0;
  computeSliding();
  findPWM(pwm);
  Eigen::Vector3d torq; torq << -u_neg_, u_pos_,0.0;
  return torq;
}

void Pendulum::findPWM(uint16_t *pwm){
  pwm[6] = lookup(6, u_pos_); // assuming that first motor is in positivve theta direction, second motor in negative
  pwm[7] = lookup(7, u_neg_);
}


void Pendulum::computeGravity(){
  u_ = pq_.L_cm / pq_.L * pq_.m * pq_.g * sin( theta_ ); // gravity component
  // std::cout << pq_.L_cm / pq_.L * pq_.m * pq_.g << std::endl;
  addUp();
}

void Pendulum::computeSliding(){
  //compute feedforward
  u_eq_ = pq_.L_cm / pq_.L * pq_.m * pq_.g * sin( theta_ ); // gravity component
  u_eq_ += pq_.Cd * fabs(d_theta_) * d_theta_ / pq_.L; // drag
  u_eq_ += pq_.I / pq_.L * ( dd_theta_des() - params_.lambd * (d_theta_ - d_theta_des() ) ); // desired speed and acceleration
  u_ = u_eq_;
  addUp();
  //compute the sliding controller
  k_ = (pq_.Cd_error * fabs(d_theta_ * d_theta_)  + pq_.I * params_.D + pq_.I * params_.nu) / pq_.L; //caclulate gain
  s_ = (d_theta_ - d_theta_des() ) + params_.lambd * ( theta_ - theta_des() );  // caclulate sliding var
  u_r_ = -1 * k_ * sat(s_ / params_.F);

  std::cout << u_r_ << "\t" << u_eq_ << "\t" << k_ << "\t" << s_ / params_.F << std::endl;
  u_ = u_r_; // add up the two components and constraint to max thrust
  addUp();
}




// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------
float Pendulum::dd_theta_des(){
  //return desired angular acceleration based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  return 0.0;
  if (tnow_ > time_limit_ || !running_){
    return 0.0;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 4.0 * M_PI / time_lim_;
  }
  else{
    return -4.0 * M_PI / time_lim_;
  }
}

float Pendulum::d_theta_des(){
  //return desired angular speed based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  return 0.0;
  if (tnow_ > time_limit_ || !running_){
    return 0.0;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 4.0 * M_PI * ( (float) tnow_/1000000 ) / pow(time_lim_, 2) ;
  }
  else{
    return 4.0*M_PI/time_lim_ - 4.0*M_PI*( (float) tnow_/1000000 ) / pow(time_lim_, 2) ;
  }
}

float Pendulum::theta_des(){
  //return desired angle based on currnt time tnow_
  // desired trajectory - go from zero to pi in time_lim_,
  // half time accelerate, half time dececlerate
  return theta_des_;
  if (tnow_ > time_limit_ || !running_){
    return M_PI;
  }
  else if (tnow_ <= time_limit_ / 2){
    return 2.0 * M_PI * ( pow( (float) tnow_/1000000, 2 ) / pow(time_lim_, 2));
  }
  else{
    return 4.0*M_PI * ((float) tnow_/1000000) / time_lim_ - 2*M_PI * pow((float) tnow_/1000000,2)/pow(time_lim_,2) - M_PI;
  }
}

float Pendulum::sat(float a){
  //saturation function
  if (a >= 1){
    return 1.0;
  }
  else if (a <= -1){
    return -1.0;
  }
  else{
    return a;
  }
}


} // ns pendulum
} // ns acl
