/**
 * @file pendulum_ros.cpp
 * @brief ROS wrapper for pendulum components
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#include "pendulum/pendulum_ros.h"

namespace acl {
namespace pendulum {

PendulumROS::PendulumROS(const ros::NodeHandle nh, const ros::NodeHandle nhp)
: nh_(nh), nhp_(nhp)
{

  init();

  //
  // Setup ROS communication
  //

  pub_imu_ = nh_.advertise<sensor_msgs::Imu>("imu", 1);
  pub_imu_filtered_ = nh_.advertise<sensor_msgs::Imu>("imu_filtered", 1);
  pub_torque_ = nh_.advertise<geometry_msgs::Vector3Stamped>("torque", 1);
  pub_motors_ = nh_.advertise<snapstack_msgs::Motors>("motors", 1);
  pub_pose_ = nh_.advertise<geometry_msgs::PoseStamped>("pose", 1);
  pub_angle_ = nh_.advertise<geometry_msgs::Vector3Stamped>("angle_deg", 1);

  srv_arm_ = nh_.advertiseService("arm", &PendulumROS::armCB, this);
  srv_isarmed_ = nh_.advertiseService("is_armed", &PendulumROS::isarmedCB, this);

  srv_runController_ = nh_.advertiseService("runController", &PendulumROS::runController, this);
  srv_setp_ = nh_.advertiseService("setP", &PendulumROS::setP, this);
  srv_setd_ = nh_.advertiseService("setD", &PendulumROS::setD, this);
  srv_setff_ = nh_.advertiseService("setFF", &PendulumROS::setFF, this);
  srv_setThetaDes_ = nh_.advertiseService("setThetaDes", &PendulumROS::setThetaDes, this);

  srv_setm6_ = nh_.advertiseService("setM6", &PendulumROS::setM6, this);
  srv_setm7_ = nh_.advertiseService("setM7", &PendulumROS::setM7, this);

  //
  // Setup the IMU communication
  //

  // When IMU data is available, let this class know via its handler
  imuman_.AddHandler(this);

  // Start handling IMU data asynchronously --- kicks off est/cntrl process
  imuman_.Initialize();
  imuman_.Start();
}

// ----------------------------------------------------------------------------

bool PendulumROS::Imu_IEventListener_ProcessSamples(sensor_imu* samples,
                                                          uint32_t count)
{
  constexpr float kNormG = 9.80665f;

  // biases for correcting first-order IMU inaccuracies (captured at startup)
  static float bacc_x = 0.0f, bacc_y = 0.0f, bacc_z = 0.0f;
  static float bgyr_x = 0.0f, bgyr_y = 0.0f, bgyr_z = 0.0f;
  constexpr size_t CALIB_COUNT = 300;

  // NOTE: we will just use the last buffered IMU sample ¯\_(ツ)_/¯
  //      (we probably won't be dropping samples anyways)
  const size_t s = count-1;

  // unpack IMU data
  uint64_t time_us = samples[s].timestamp_in_us;
  float acc[3] = { 0.0f };
  float gyr[3] = { 0.0f };

  // Using original IMU orientation. See ATLFlight docs for image.
  // (https://github.com/ATLFlight/ros-examples#coordinate-frame-conventions)
  acc[0] = samples[s].linear_acceleration[0] * kNormG;
  acc[1] = samples[s].linear_acceleration[1] * kNormG;
  acc[2] = samples[s].linear_acceleration[2] * kNormG;
  gyr[0] = samples[s].angular_velocity[0];
  gyr[1] = samples[s].angular_velocity[1];
  gyr[2] = samples[s].angular_velocity[2];

  // TODO: calibrate IMU on startup
  static size_t n = 0;
  if (!is_imu_calibrated_) {

    // TODO: +gravity is removed from the y accel value since y is pointing up.
    // Instead of hardcoding this into the code, we could be smarter.

    n++;
    bacc_x += acc[0];
    bacc_y += acc[1] - kNormG;
    bacc_z += acc[2];
    bgyr_x += gyr[0];
    bgyr_y += gyr[1];
    bgyr_z += gyr[2];

    if (n >= CALIB_COUNT) {
      // average the noise to calculate a constant bias term
      bacc_x /= static_cast<float>(n);
      bacc_y /= static_cast<float>(n);
      bacc_z /= static_cast<float>(n);
      bgyr_x /= static_cast<float>(n);
      bgyr_y /= static_cast<float>(n);
      bgyr_z /= static_cast<float>(n);
      is_imu_calibrated_ = true;
      ROS_INFO("IMU calibration complete");
    }
  } else {
    // remove constant biases
    acc[0] -= bacc_x;
    acc[1] -= bacc_y;
    acc[2] -= bacc_z;
    gyr[0] -= bgyr_x;
    gyr[1] -= bgyr_y;
    gyr[2] -= bgyr_z;

    process(time_us, acc, gyr);
  }

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void PendulumROS::process(uint64_t time_us, float acc[3], float gyr[3])
{

  // map raw buffer to Eigen datatype
  Eigen::Map<Eigen::Vector3f> accVec(acc);
  Eigen::Map<Eigen::Vector3f> gyrVec(gyr);
  // uint16_t pwm[EscMan::NUM_PWMS] = { 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };
  uint16_t pwm[EscMan::NUM_PWMS] = { 0,0,0,0, 0,0,0,0};
  // if (pendulum_.time_start_ == 0 && escman_.isArmed() ){
  //   pendulum_.time_start_ = time_us;
  // }

  //
  // Main control loop
  //



  // allow pendulum to estimate its current angle given IMU data
  pendulum_.estimateAngle(time_us, accVec, gyrVec);

  // std::cout << "Before c: " << pwm[5] << " " << pwm[6] << " " << pwm[7] << std::endl;
  // calculate the desired torque and mix down to PWM signals
  const auto torque = pendulum_.computeControl(pwm);
  // std::cout << "Computed: " << pwm[5] << " " << pwm[6] << " " << pwm[7] << std::endl;

  // achieve the desired torque by actuating motors via desired pwm
  escman_.update(pwm);
  // std::cout << "post run: " << pwm[5] << " " << pwm[6] << " " << pwm[7] << std::endl;

  //
  // Publish signals for logging
  //

  { // raw IMU
    sensor_msgs::Imu msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "imu_link";
    msg.angular_velocity.x = gyrVec.x();
    msg.angular_velocity.y = gyrVec.y();
    msg.angular_velocity.z = gyrVec.z();
    msg.linear_acceleration.x = accVec.x();
    msg.linear_acceleration.y = accVec.y();
    msg.linear_acceleration.z = accVec.z();
    pub_imu_.publish(msg);
  }

  { // filtered IMU
    sensor_msgs::Imu msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "imu_link";
    msg.angular_velocity.x = pendulum_.getFilteredGyro().x();
    msg.angular_velocity.y = pendulum_.getFilteredGyro().y();
    msg.angular_velocity.z = pendulum_.getFilteredGyro().z();
    msg.linear_acceleration.x = pendulum_.getFilteredAccel().x();
    msg.linear_acceleration.y = pendulum_.getFilteredAccel().y();
    msg.linear_acceleration.z = pendulum_.getFilteredAccel().z();
    pub_imu_filtered_.publish(msg);
  }

  { // estimated pose
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.pose.position.x = 0.0;
    msg.pose.position.y = 0.0;
    msg.pose.position.z = 0.0;
    msg.pose.orientation.w = pendulum_.getQuaternion().w();
    msg.pose.orientation.x = pendulum_.getQuaternion().x();
    msg.pose.orientation.y = pendulum_.getQuaternion().y();
    msg.pose.orientation.z = pendulum_.getQuaternion().z();
    pub_pose_.publish(msg);
  }

  { // broadcast estimated pose on tf tree
    geometry_msgs::TransformStamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.child_frame_id = "arm";
    msg.transform.translation.x = 0.0;
    msg.transform.translation.y = 0.0;
    msg.transform.translation.z = 0.0;
    msg.transform.rotation.w = pendulum_.getQuaternion().w();
    msg.transform.rotation.x = pendulum_.getQuaternion().x();
    msg.transform.rotation.y = pendulum_.getQuaternion().y();
    msg.transform.rotation.z = pendulum_.getQuaternion().z();
    br_.sendTransform(msg);
  }

    { // extracted estimated angle for convenience
    geometry_msgs::Vector3Stamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.vector.x = pendulum_.getAngle() * 180.0/M_PI;
    msg.vector.y = 0.0;
    msg.vector.z = 0.0;
    pub_angle_.publish(msg);
  }

  { // desired torque
    geometry_msgs::Vector3Stamped msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "base_link";
    msg.vector.x = torque.x();
    msg.vector.y = torque.y();
    msg.vector.z = torque.z();
    pub_torque_.publish(msg);
  }

  { // motor pwm commands
    snapstack_msgs::Motors msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.m7 = pwm[6];
    msg.m8 = pwm[7];
    pub_motors_.publish(msg);
  }
}

// ----------------------------------------------------------------------------

void PendulumROS::init()
{
  Pendulum::Params params;
  nhp_.param<float>("lpf/acc/alpha", params.lpf_acc_alpha, 0.0);
  nhp_.param<float>("lpf/gyr/alpha", params.lpf_gyr_alpha, 0.0);
  // nhp_.param<float[]>("thrust", params.thrust, []);
  // nhp_.param<float[]>("esc/m1", params.esc_m1, []);
  // nhp_.param<float[]>("esc/m2", params.esc_m2, []]);
  // params.thrust = {0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.0, 2.05, 2.1, 2.15, 2.2};
  // params.esc_m1 = {1023, 1028, 1055, 1084, 1104, 1125, 1153, 1177, 1193, 1207, 1224, 1244, 1259, 1273, 1287, 1301, 1317, 1329, 1341, 1354, 1366, 1377, 1388, 1403, 1417, 1429, 1441, 1456, 1469, 1481, 1492, 1506, 1520, 1532, 1543, 1554, 1567, 1582, 1599, 1612, 1624, 1635, 1647, 1659, 1673};
  // params.esc_m2 = {1023, 1051, 1079, 1106, 1131, 1147, 1161, 1180, 1203, 1225, 1239, 1250, 1261, 1273, 1287, 1300, 1311, 1321, 1330, 1341, 1355, 1371, 1385, 1398, 1411, 1424, 1440, 1456, 1470, 1484, 1498, 1512, 1525, 1536, 1550, 1566, 1583, 1597, 1612, 1629, 1641, 1651, 1661, 1671, 1685, 1701, 1715, 1727, 1738, 1749};
  nhp_.param<float>("theta/m6", params.m6, 0.0);
  nhp_.param<float>("theta/m7", params.m7, 0.0);
  nhp_.param<float>("pend/lambda", params.lambd, 0.0);
  nhp_.param<float>("pend/nu", params.nu, 0.0);
  nhp_.param<float>("pend/D", params.D, 0.0);
  nhp_.param<float>("pend/F", params.F, 0.0);
  nhp_.param<float>("pend/off", params.off, 0.0);
  nhp_.param<float>("pend/multip", params.multip, 0.0);
  pendulum_.setParameters(params);
}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

bool PendulumROS::armCB(std_srvs::SetBool::Request &req,
                        std_srvs::SetBool::Response &res)
{
  bool arm_requested = req.data;
  if (arm_requested && is_imu_calibrated_) {
    bool success = escman_.arm();
    if (!success) res.message = "Failed to arm";
    else res.message = "ARMED";
  } else {
    bool success = escman_.disarm();
    if (!success) res.message = "Failed to disarm";
    else res.message = "DISARMED";
  }

  res.success = escman_.isArmed();
  return true;
}

// ----------------------------------------------------------------------------

bool PendulumROS::isarmedCB(std_srvs::Trigger::Request &req,
                            std_srvs::Trigger::Response &res)
{
  res.success = escman_.isArmed();
  return true;
}

// ----------------------------------------------------------------------------

bool PendulumROS::runController(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setMultip(req.data);
  res.success = true;
  return true;
}


bool PendulumROS::setP(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setP(req.data);
  res.success = true;
  return true;
}

bool PendulumROS::setD(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setD(req.data);
  res.success = true;
  return true;
}

bool PendulumROS::setFF(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setFF(req.data);
  res.success = true;
  return true;
}

bool PendulumROS::setThetaDes(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setThetaDes(req.data * M_PI/180);
  res.success = true;
  return true;
}

bool PendulumROS::setM6(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setM6(req.data);
  res.success = true;
  return true;
}

bool PendulumROS::setM7(savvas_msgs::srv_float::Request &req,
                        savvas_msgs::srv_float::Response &res)
{
  pendulum_.setM7(req.data);
  res.success = true;
  return true;
}

// bool PendulumROS::runController(std_srvs::SetBool::Request &req,
//                         std_srvs::SetBool::Response &res)
// {
//   bool start_requested = req.data;
//   if (start_requested && escman_.isArmed()) { //start controller
//     pendulum_.run();
//     res.success = true;
//     res.message = "Controller started.";
//   } else if (start_requested && !escman_.isArmed()) { //start controller and escs
//     bool success = escman_.arm();
//     if (!success) {
//       res.message = "Failed to arm ESCs, controller not started";
//       res.success = false;
//     }
//     else {
//       res.message = "ESCs armed, controller started";
//       res.success = true;
//       pendulum_.run();
//     }
//   } else {
//     pendulum_.stopRunning();
//     res.message = "Controller reset";
//     res.success = true;
//   }
//   return true;
// }


} // ns pendulum
} // ns acl
