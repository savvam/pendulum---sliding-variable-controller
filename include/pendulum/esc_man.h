/**
 * @file esc_man.h
 * @brief Handles ESC signaling for motors
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <memory>
#include <mutex>

#include <esc_interface/esc_interface.h>

namespace acl {
namespace pendulum {

  class EscMan {
  public:
    static constexpr uint8_t NUM_PWMS = 8;

  public:
    EscMan();
    ~EscMan();

    void update(uint16_t f[NUM_PWMS]);

    // hooks into esc lib
    bool arm();
    bool disarm();
    bool isArmed();

  private:
    std::unique_ptr<acl::ESCInterface> escs_;
    std::mutex mutex_; ///< prevent pwm updates while arming/disarming
};

} // ns pendulum
} // ns acl
