/**
 * @file pendulum.h
 * @brief Pendulum control and estimation
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 Dec 2019
 */

#pragma once

#include <cstdint>

#include <Eigen/Dense>
#include <cmath>
#include <iostream>

namespace acl {
namespace pendulum {

  class Pendulum
  {
  public:

    struct PID_params {
      float Kp, Ki, Kd, ff;
    };

    struct Params {
      /// \brief Lowpass filter parameters, in [0 1]. Zero means no filtering.
      float lpf_acc_alpha; ///< LPF param for accel meas.
      float lpf_gyr_alpha; ///< LPF param for gyro meas.
      // float thrust[];
      float m6;
      float m7;
      float lambd;
      float nu;
      float F = M_PI/16; // F - dimaeter of the tube
      float D = M_PI/4; // error in the dynamics
      float off;
      float multip;
    };

    struct Physical_Quantities {
      /// physical quantitites that define  the pendulum
      float g = 9.81; // m/s2
      float m = 0.214; // kg
      float L_cm = 0.0921; //m, cm of pendulum
      float L = 0.229; // m, where torque is applied
      float I = 0.00417; // kgm2 inertia; assuming that all mass is at the tip
      float Cd = 0.000077; //estimated drag
      // float Cd_true = 0.0001 # true drag
      float Cd_error = 0.01; //error estimate for drag, waaaay overkilled
      // float lambd = 2; // lambda for sliding variable; - the higher - the better
      // float F = M_PI/100; // 1/s
      // float nu = 0.1; // 1/s2
      // float D = M_PI/4; // 1/s2
      float umax = 1.7; // N, max allowed force
      // float a = 286.8;
      // float a = 400;
      // float b = 1065.5;
    };

  public:
    Pendulum();
    ~Pendulum() = default;

    void setM6(float a);
    void setM7(float a);
    void setLambd(float a);
    void setNu(float a);
    void setF(float a);
    void setD(float a);
    void setThetaDes(float a);
    void setMultip(float a);
    void setParameters(const Params& params);
    void run();
    void stopRunning();
    int lookup(int m_num, float thrust);

    void estimateAngle(uint64_t time_us, const Eigen::Vector3f& acc,
                      const Eigen::Vector3f& gyr);

    Eigen::Vector3d computeControl(uint16_t * pwm);
    void findPWM(uint16_t *pwm);
    void addUp();
    void computePID();
    void computeSliding();
    void computeGravity();


    /// \brief Getters for filtered IMU data
    const Eigen::Vector3f& getFilteredAccel() const { return acc_; }
    const Eigen::Vector3f& getFilteredGyro() const { return gyr_; }
    const Eigen::Quaterniond& getQuaternion() const { return quat_; }
    const double& getAngle() const { return theta_; }
    uint64_t time_start_ = 0;
    uint64_t tnow_; // time passed since time_start
    double theta_; ///< roll angle (x-axis) extracted from quat
    double d_theta_; ///< roll angular velocity
    float s_; // sliding variable
    float k_; // gain
    float u_r_; //sliding tube thrust component
    float u_eq_; //feedforward thrust component
    float u_, u_neg_, u_pos_; // thrust output
    float thrust_[44] = {0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.0, 2.05, 2.1, 2.15};
    //m2? float esc_m1_[44] = {1023, 1051, 1079, 1106, 1131, 1147, 1161, 1180, 1203, 1225, 1239, 1250, 1261, 1273, 1287, 1300, 1311, 1321, 1330, 1341, 1355, 1371, 1385, 1398, 1411, 1424, 1440, 1456, 1470, 1484, 1498, 1512, 1525, 1536, 1550, 1566, 1583, 1597, 1612, 1629, 1641, 1651, 1661, 1671};
    //m1? float esc_m1_[44] = {1023, 1028, 1055, 1084, 1104, 1125, 1153, 1177, 1193, 1207, 1224, 1244, 1259, 1273, 1287, 1301, 1317, 1329, 1341, 1354, 1366, 1377, 1388, 1403, 1417, 1429, 1441, 1456, 1469, 1481, 1492, 1506, 1520, 1532, 1543, 1554, 1567, 1582, 1599, 1612, 1624, 1635, 1647, 1659};
    //m4 float esc_m1_[44] = {1023, 1026, 1044, 1070, 1093, 1113, 1131, 1152, 1174, 1190, 1206, 1220, 1236, 1251, 1263, 1275, 1287, 1300, 1312, 1324, 1336, 1349, 1360, 1370, 1379, 1390, 1403, 1418, 1430, 1440, 1450, 1462, 1476, 1492, 1505, 1515, 1527, 1537, 1548, 1560, 1573, 1587, 1600, 1612};
    //m5 float esc_m1_[44] = {1023, 1026, 1033, 1062, 1088, 1108, 1126, 1149, 1172, 1188, 1202, 1217, 1234, 1249, 1263, 1277, 1290, 1304, 1317, 1329, 1342, 1354, 1365, 1376, 1389, 1403, 1417, 1428, 1437, 1447, 1457, 1471, 1486, 1497, 1510, 1523, 1535, 1545, 1554, 1565, 1580, 1595, 1608, 1619};
    //m6 float esc_m1_[44] = {1024, 1028, 1054, 1080, 1102, 1121, 1143, 1165, 1182, 1196, 1213, 1231, 1245, 1260, 1273, 1288, 1301, 1313, 1325, 1338, 1352, 1364, 1375, 1386, 1397, 1409, 1424, 1435, 1446, 1456, 1467, 1477, 1491, 1506, 1520, 1531, 1541, 1551, 1564, 1577, 1592, 1603, 1614, 1625};
    float esc_m6_[44] = {1023, 1026, 1033, 1062, 1088, 1108, 1126, 1149, 1172, 1188, 1202, 1217, 1234, 1249, 1263, 1277, 1290, 1304, 1317, 1329, 1342, 1354, 1365, 1376, 1389, 1403, 1417, 1428, 1437, 1447, 1457, 1471, 1486, 1497, 1510, 1523, 1535, 1545, 1554, 1565, 1580, 1595, 1608, 1619};
    float esc_m7_[44] = {1023, 1026, 1044, 1070, 1093, 1113, 1131, 1152, 1174, 1190, 1206, 1220, 1236, 1251, 1263, 1275, 1287, 1300, 1312, 1324, 1336, 1349, 1360, 1370, 1379, 1390, 1403, 1418, 1430, 1440, 1450, 1462, 1476, 1492, 1505, 1515, 1527, 1537, 1548, 1560, 1573, 1587, 1600, 1612};


    float theta_des_ = M_PI/2;
    bool running_ = false;
    PID_params pid_;
    Params params_; ///< current parameters

  private:

    Physical_Quantities pq_; /// current physical parameters of the pendulum

    /// \brief Internal state
    bool lpf_initialized_ = false; ///< so we can use first measurement
    Eigen::Vector3f acc_; ///< filtered accel data
    Eigen::Vector3f gyr_; ///< filtered gyro data
    Eigen::Quaterniond quat_; ///< base_link (pivot point) w.r.t base

    // uint64_t time_start = 10000000; // time that task started - start at 10 sec

    float time_lim_ = 1; // time limit in seconds
    uint64_t time_limit_ = time_lim_ * 1000000; // get  up in 1 second, in microseconds

    float dd_theta_des();
    float d_theta_des();
    float theta_des();
    float sat(float a);
    /**
     * @brief      First-order lowpass filter following
     *
     *                y = alpha*y + (1-alpha)*x
     *
     * @param[in]  alpha  LPF parameter in [0 1]. Zero means no filtering.
     * @param[in]  meas   The new measurement, x.
     * @param      out    The current filtered value and output, y.
     *
     * @tparam     T      The templated type of the signal to LPF.
     */
    template<typename T>
    static void LPF(float alpha, const T& meas, T& out)
    {
      out = alpha*out + (1-alpha)*meas;
    }
  };

} // ns pendulum
} // ns acl
